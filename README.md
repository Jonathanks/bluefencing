# Geofencing library

Bluefencing is a hosted geofencing library that offers two capabilities:

1. Creating a geopoint from an address or a place ID using Google Maps Geocoding API.
2. Geofencing a set of points given as an array of GeoJSON values.

You can see the API in action at https://bluefencing.herokuapp.com/.

The first capability is currently not functional because I did not provide a Google Maps API Key. You can run the application with your own key to test it.

The second capability can be tested by sending the following request to the hosted API.

```
POST https://bluefencing.herokuapp.com/geofences/ HTTP 1.1
Accept: application/json
Content-Type: application/json

{
	"coordinates": [
		[
			-10.8,
			5.31
		],
		[
			20.9,
			30.5
		]
	]
}

{
  "message": "Geofence data",
  "status": "success",
  "data": [
    {
      "point": {
        "latitude": 5.31,
        "longitude": -10.8
      },
      "radius": 250
    },
    {
      "point": {
        "latitude": 30.5,
        "longitude": 20.9
      },
      "radius": 250
    }
  ]
}
```

### Run it yourself

You can run the application yourself using either Docker or a system installation of `node` and `npm`.
First clone the project onto your computer.

#### Using `node` and `npm`

1. In the root directory, run `npm install` to fetch the dependencies, then run `npm build` to build the application.
2. You may run the tests using `npm test`.
3. Start the application by running `npm start`.

#### Using Docker

1. Build a Docker image of the application by running `docker build -t bluefencing:latest .`
2. Run the docker image using `docker run [...the options you want to pass] -e "PORT=<PORT>" bluefencing:latest`
