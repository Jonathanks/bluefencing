import {
  Client,
  GeocodeRequest,
  GeocodeResponse,
  LatLngLiteral,
  Status,
} from "@googlemaps/google-maps-services-js";
import {
  Either,
  getApplicativeValidation,
  left,
  mapLeft,
  right,
} from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import * as D from "io-ts/Decoder";
import { getSemigroup } from "io-ts/lib/FreeSemigroup";
import { Eq } from "fp-ts/lib/Eq";
import { chain, tryCatch, fromEither, TaskEither } from "fp-ts/lib/TaskEither";

export interface LatitudeBrand {
  readonly Latitude: unique symbol;
}

export type Latitude = number & LatitudeBrand;

export interface LongitudeBrand {
  readonly Longitude: unique symbol;
}

export type Longitude = number & LongitudeBrand;

export const _Latitude = pipe(
  D.number,
  D.refine((n): n is Latitude => n >= 0 && n <= 90, "Latitude")
);

export const Latitude = D.withMessage((input) => `${input} is not a latitude`)(
  _Latitude
);

export const _Longitude = pipe(
  D.number,
  D.refine((n): n is Longitude => n >= -180 && n <= 180, "Longitude")
);

export const Longitude = D.withMessage(
  (input) => `${input} is not a longitude`
)(_Longitude);

export interface GeoPoint {
  latitude: Latitude;
  longitude: Longitude;
}

export const eqGeoPoint: Eq<GeoPoint> = {
  equals(x, y) {
    return x.latitude === y.latitude && x.longitude === y.longitude;
  },
};

export const _GeoPoint = D.struct({
  latitude: Latitude,
  longitude: Longitude,
});

const getGeoPointErrorMessage = (
  input: { latitude: any; longitude: any },
  _: D.DecodeError
): string => `${input.latitude},${input.longitude} is not a valid geopoint`;

export const GeoPoint = D.withMessage(getGeoPointErrorMessage)(_GeoPoint);

export const zero: GeoPoint = {
  latitude: 0 as Latitude,
  longitude: 0 as Longitude,
};

const geoPointValidation = getApplicativeValidation<D.DecodeError>(
  getSemigroup()
);

const makeGeoPoint = (latitude: Latitude) => (
  longitude: Longitude
): GeoPoint => ({ latitude, longitude });

export function fromGeoJSON([longitude, latitude]: [number, number]) {
  return pipe(
    geoPointValidation.ap(right(makeGeoPoint), Latitude.decode(latitude)),
    (v) => geoPointValidation.ap(v, Longitude.decode(longitude))
  );
}

export type GeocodingError =
  | {
      type: "maps_error_response";
      message: string;
      status: Status;
    }
  | {
      type: "invalid_geopoint";
      value?: LatLngLiteral;
    }
  | {
      type: "network_error";
      error: Error;
    };

export const geopointFromGoogleMaps = (client: Client) => (
  params: GeocodeRequest
): TaskEither<GeocodingError, GeoPoint> => {
  const geocode = tryCatch(
    () => client.geocode(params),
    (error) => ({ type: "network_error", error } as GeocodingError)
  );

  return pipe(
    geocode,
    chain((v) => fromEither(geopointFromGeocodeResponse(v)))
  );
};

function geopointFromGeocodeResponse(
  response: GeocodeResponse
): Either<GeocodingError, GeoPoint> {
  switch (response.data.status) {
    case Status.ZERO_RESULTS: {
      return left({ type: "invalid_geopoint" });
    }

    case Status.OK: {
      const location = response.data.results[0]?.geometry.location;

      const decodeResult = pipe(
        geoPointValidation.ap(
          right(makeGeoPoint),
          Latitude.decode(location?.lat)
        ),
        (v) => geoPointValidation.ap(v, Longitude.decode(location?.lng))
      );

      return pipe(
        decodeResult,
        mapLeft((_) => ({
          type: "invalid_geopoint",
          value: location,
        }))
      );
    }

    default:
      return left({
        type: "maps_error_response",
        status: response.data.status,
        message: response.data.error_message,
      });
  }
}
