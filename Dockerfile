FROM node:lts-alpine

ADD ./*.ts package*.json tsconfig.json /srv/

WORKDIR /srv/

RUN npm install

RUN npm run build

CMD ["npm", "run", "start"]
