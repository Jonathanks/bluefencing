import * as fc from "fast-check";
import { identity, pipe } from "fp-ts/lib/function";
import { GeoPoint, Latitude, Longitude } from "./geopoint";
import { Either, fold as foldEither } from "fp-ts/lib/Either";
import haversineDistance from "haversine-distance";
import { buildGeofence } from "./geofence";

const valueOrThrow: <E, A>(_: Either<E, A>) => A = foldEither((e) => {
  throw e;
}, identity);

const latitudeArb = fc
  .float()
  .map((v) =>
    pipe(Math.min(v * 90.000_000_000_001, 90), Latitude.decode, valueOrThrow)
  );

const longitudeArb = fc
  .float()
  .map((v) => pipe(-180 * v + 180, Longitude.decode, valueOrThrow));

const geopointArb = fc.tuple(latitudeArb, longitudeArb).map(
  ([latitude, longitude]): GeoPoint => {
    return { latitude, longitude };
  }
);

export const geofenceArb = fc.array(geopointArb);

const overlappingGeofenceArb = fc.nat().chain((radius) =>
  fc
    .tuple(geopointArb, geopointArb)
    .filter(([first, second]) => haversineDistance(first, second) <= radius)
    .map((v) => buildGeofence(v, radius))
);

describe("Geofence construction tests", () => {
  test("overlapping geofences are resized to respect the minimum spacing", () => {
    fc.assert(
      fc.property(overlappingGeofenceArb, ([first, second]) => {
        if (first && second) {
          const distanceApart = haversineDistance(first.point, second.point);
          expect(distanceApart).toBeGreaterThan(first.radius + second.radius);
        }
      }),
      {
        endOnFailure: true,
      }
    );
  });
});
